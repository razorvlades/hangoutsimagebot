This is a simple script to send all images in a folder to a conversation on hangouts. <br>

Requires python 3.6.x (preferably 3.6.6, 3.7.0 will not work)

<b> Dependencies: </b>

* hangups <br>
`pip install hangups`

* asyncio <br>
`pip install asyncio`

<b> Run it: </b> <br>
`python main.py`
<br><br>
If you are unable to login (sometimes it doesn't like the 2 factor auth) follow these steps to manually login: <br>

1. Run `hangups_manual_login.py`. It requires hangups to be installed.
2. Open the URL provided by the script in your browser.
3. If prompted, log into your Google account normally.
4. You should be redirected to a page that says "One moment please...". Copy the oauth_code cookie value set by this page (instructions below for Chrome): <br>
    i. Press F12 to open DevTools. <br>
    ii. Select the "Application" tab. <br>
    iii. In the sidebar, expand "Cookies" and select https://accounts.google.com. <br>
    iv. In the cookie list, double click on the value for the oauth_code cookie to select it, and copy the value. <br>
5. Paste the value back into the script's prompt and press enter. <br>

This will create a refresh_token.txt in the main script directory which will auto log you in when you run the script.
