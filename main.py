from hangups import *
import sys
import asyncio
import os
import glob


class Bot(object):

    def __init__(self, token_path):
        self.token_path = token_path
        self._client = None
        self._conv_list = None
        self._user_list = None
        self._handlers = None

    def login(self, token_path):
        try:
            token = get_auth_stdin(token_path)
            return token

        except GoogleAuthError as e:
            print("LOGIN FAILED")
            return False

    def run(self):
        login = self.login(self.token_path)
        if login:

            loop = asyncio.get_event_loop()

            for retry in range(5):
                try:
                    self._client = Client(login)
                    self._client.on_connect.add_observer(self._on_connect)
                    self._client.on_disconnect.add_observer(self._on_disconnect)

                    loop.run_until_complete(self._client.connect())
                    print("bot is exiting")

                    sys.exit(0)

                except Exception as e:
                    print("Error, exiting")

        sys.exit(1)

    def stop(self):
        """Disconnect from Hangouts"""
        asyncio.async(
            self._client.disconnect()
        ).add_done_callback(lambda future: future.result())

    @asyncio.coroutine
    async def _on_connect(self):
        print("Connected to Hangouts")

        self.user_list, self.conv_list = (
            await build_user_conversation_list(self._client)
        )
        convos = self.conv_list.get_all()

        print("Conversations:\n")
        i = 1
        for convo in convos:
            print(str(i) + ": ", end='')
            j = 0
            for usr in convo.users:
                if not usr.is_self:
                    if len(convo.users) > 2:
                        if j == len(convo.users) - 2:
                            print(usr.full_name)
                        else:
                            print(usr.full_name + ", ", end='')
                    else:
                        print(usr.full_name)
                    j = j + 1
            i = i + 1

        while True:
            number = input("Enter conversation number to send images to: ")
            if not number:
                print("Please enter a number")
            elif 0 < int(number) <= len(convos):
                break
            else:
                print("Please enter a valid conversation number")
        while True:
            path = input("Enter path to folder to send all images from: ")
            if path:
                break
            else:
                print("Please enter a folder path")
        print("Sending images...")

        conv = convos[int(number) - 1]
        if not path.endswith('/'):
            path = path + "/"
        filetypes = ('*.png', '*.jpg', '*.jpeg')
        for filetype in filetypes:
            for image in glob.glob(os.path.join(path, filetype)):
                imageFile = open(image, "rb")
                await conv.send_message([], image_file=imageFile)
                print("Sent: " + image)
        print("Done sending images!")
        self.stop()

    def _on_disconnect(self):
        print('Connection lost!')


def main():
    bot = Bot('refresh_token.txt')
    bot.run()


if __name__ == "__main__":
    main()
